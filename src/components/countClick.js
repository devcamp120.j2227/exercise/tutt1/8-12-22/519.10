import { useEffect, useState } from "react";

const CountClick = () => {
    const [count, setCount] = useState(0);

    const clickHandler = () => {
        setCount(count + 1)
    }

    useEffect(() => {
        document.title = `You click ${count} times`;
        console.log("Test");
    }, []);

    return (
        <div>
            <div>You clicked {count} times</div>
            <button onClick={clickHandler}>Click me!</button>
        </div>
    )
}

export default CountClick;